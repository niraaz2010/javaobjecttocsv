package test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class FileConvert {
    private String fileName;
    File file;
    FileWriter fileWriter;
    BufferedWriter bufferWriter;
    FileReader fileReader;
    BufferedReader bufferReader;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public FileConvert(String fileName) {
        this.fileName = fileName;
    }

    public FileConvert() {
    }

    public void objToCsv(Student student) {
        try {
            file = new File(this.fileName);
            fileWriter = new FileWriter(file);

            if (!file.exists()) {
                file.createNewFile();
            }

            bufferWriter = new BufferedWriter(fileWriter);
            System.out.println(student.toString());

        }catch (Exception e) {
            System.err.println("File is not created");
        } finally {

            if (bufferWriter != null) {
                try {
                    bufferWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fileWriter != null) {
                try {
                    fileWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public Student csvToObj(){
        Student student = new Student();
        String singleLine;
        try {
            file = new File(this.fileName);
            fileReader = new FileReader(file);
            bufferReader = new BufferedReader(fileReader);
            while((singleLine = bufferReader.readLine())!=null) {
                List<String> result = Arrays.asList(singleLine.split("\\s*,\\s*"));
                student.setFirstName(result.get(0));
                student.setLastName(result.get(1));
                student.setRollNumber(Integer.parseInt(result.get(2)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if (bufferReader != null) {
                try {
                    bufferReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fileReader != null) {
                try {
                    fileReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return student;
    }


}
