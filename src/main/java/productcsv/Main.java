package productcsv;

import productcsv.entities.Product;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        String COMMA_DELIMITER= ",";
        String NEW_LINE_SEPARATOR="\n";
        String FILE_HEADER="id,name,price,quantity";
        FileWriter fileWriter=new FileWriter("src\\main\\java\\productcsv\\data\\product.csv");
        try{
            List<Product> productList=new ArrayList<Product>();
            productList.add(new Product("P01","name1",1000,2));
            productList.add(new Product("P02","name2",2000,3));
            productList.add(new Product("P03","name3",3000,4));
            productList.add(new Product("P04","name4",5000,3));

            fileWriter.append(FILE_HEADER);
            for(Product p:productList){
                fileWriter.append(NEW_LINE_SEPARATOR);
                fileWriter.append(p.getId());
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(p.getName());
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(String.valueOf(p.getPrice()));
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(String.valueOf(p.getQuantity()));

            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        fileWriter.flush();
        fileWriter.close();
    }
}
